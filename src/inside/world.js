export class world {
	constructor(name) {
		this.name = name
	}

	yell(st) {
		console.log(`${this.name} yells: ${st}`)
	}
}